import React, { useState, useRef, useEffect } from 'react';

function LockPanel() {
    const [isLocked, setIsLocked] = useState(true); // Estado inicial: candado cerrado
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [password, setPassword] = useState('');
    const passwordInputRef = useRef(null);
    const handleKeyUp = (event) => {
        if (event.keyCode === 13) {
            closeModal();
        }
    };

    useEffect(() => {
        if (isModalOpen) {
            passwordInputRef.current.focus();
        }
        passwordInputRef.current.addEventListener('keyup', handleKeyUp);
        return () => {
            // Limpia el event listener cuando el componente se desmonta
            passwordInputRef.current.removeEventListener('keyup', handleKeyUp);
        };
    }, [isModalOpen]);

    const toggleLock = () => {
        // console.log(isLocked)
        if (isLocked) {
            setIsModalOpen(true);

            document.querySelector('.modal').classList.add('show');
        } else {
            setIsModalOpen(false);
            document.querySelector('.modal').classList.remove('show');
            setIsLocked(true); // Cambia el estado del candado

        }
        // setIsLocked(!isLocked); // Cambia el estado del candado
    };

    const closeModal = () => {
        document.querySelector('.modal').classList.remove('show');
        setIsModalOpen(false);
        setIsLocked(false); // Cambia el estado del candado
        setPassword("");
        passwordInputRef.current.blur();

    };
    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };
    return (
        <div className="lock-panel">
            {/* <div className={`lock-panel-element ${isLocked ? 'locked' : 'unlocked'}`} onClick={toggleLock}> */}
            <div className={`lock-panel-element`} onClick={toggleLock}>

                {/* Contenido del candado */}
                {isLocked ?
                    <i className="circle fa fa-lock"></i>
                    : <i className="circle fa fa-unlock open"></i>
                }
            </div>
            {/* {isModalOpen && ( */}
            <div className="modal">
                <div className='modal-container'>
                    {/* <div className='modal-title'>Ingresa la contraseña</div> */}
                    <input
                        className='modal-input'
                        type="password"
                        value={password}

                        onChange={handlePasswordChange}
                        ref={passwordInputRef}
                    // placeholder="Contraseña"
                    />
                    <i className="key-icon circle fa fa-lock"></i>
                    {/* <i className="ok-icon circle fa fa-paper-plane" onClick={closeModal}></i> */}
                    {/* <i className="ok-icon circle fa fa-paper-plane" onClick={closeModal}></i> */}
                    <div className='modal-button-ok' onClick={closeModal}>Ok</div>

                    {/* <div className='modal-button' onClick={closeModal}>Aceptar</div> */}
                </div>
            </div>
            {/* )} */}
        </div>
    );
}

export default LockPanel;
