import React from 'react';
import LockPanel from './LockPanel';


function Element({ label }) {


    return (
        <div className={"right-panel-element"}>
            <i className={`circle fa fa-video-camera`}></i>
            <p>{label}</p>
            <i className={`arrow fa fa-angle-right`}></i>
        </div>
    );
}

function RightPanel() {
    return (
        <div className="right-panel">
            <LockPanel />

            <div className='right-panel-cameras'>
                <Element label={""}></Element>
                <Element label={""}></Element>
                <Element label={""}></Element>
            </div>
        </div>
    );
}

export default RightPanel;
