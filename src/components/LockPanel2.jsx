import React from 'react';

function Element({ icon }) {
    return (
        <div className={"lock-panel-element"}>
            <i className={`circle fa ${icon}`}></i>
        </div>
    );
}

function LockPanel() {
    return (
        <div className="lock-panel">
            <Element icon={"fa-lock"}></Element> 
            <Element icon={"fa-unlock"}></Element> 
        </div>
    );
}

export default LockPanel;
