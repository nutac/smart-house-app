import React, { useState } from 'react';
import Modal from 'react-modal';
Modal.setAppElement('#root');

function LockPanel() {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isUnlocked, setIsUnlocked] = useState(false);
    const [password, setPassword] = useState('');

    const openModal = () => {
        if (!isUnlocked) {
            setIsModalOpen(true);
        } else {
            setIsUnlocked(false)
        }
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const unlock = () => {
        setIsUnlocked(true);
        closeModal();
    };

    return (
        <div className="lock-panel">
            <div className="lock-panel-element" onClick={openModal}>
                <i className=
                    {`circle fa ${isUnlocked ? 'fa-unlock' : 'fa-lock'}`}>
                </i>
            </div>
            <Modal isOpen={isModalOpen && !isUnlocked}
                onRequestClose={closeModal}
                contentLabel="Password Modal"
                className="modal"
            >
                <div className='modal-container'></div>
                <h2>Ingresa la contraseña:</h2>
                <input
                    type="password"
                    value={password}
                    onChange={handlePasswordChange}
                    placeholder="Contraseña"
                />
                <button onClick={unlock}>Aceptar</button>
            </Modal>
        </div>
    );
}

export default LockPanel;
