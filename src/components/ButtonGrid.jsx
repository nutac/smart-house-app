import React, { useState } from 'react';
import Slider from './Slider';

function ButtonGrid() {
    const labels = [
        "living",
        "cocina",
        "habitación",
        "hall",
        "baño",

        "aire acond.",
        "música",
        "t.v.",
        "blackout",
        "speakers",

        "cocina",
        "habitación",
        "baño",
        "lavadero",
        "hall"
    ]
    const icons = [
        'far fa-lightbulb',
        'far fa-lightbulb',
        'far fa-lightbulb',
        'far fa-lightbulb',
        'far fa-lightbulb',

        'fa fa-thermometer-half',
        'fa fa-music',
        'fa fa-tv',
        'fa fa-window-maximize',
        'fa fa-volume-up',

        'fa fa-cutlery ',
        'fa fa-bed',
        'fa fa-shower',
        'fa fa-tint',
        'fa fa-archway'

        // 'fa-arrows-alt'
        // 'fa-times',
        // 'fa-volume-up',
        // 'fa-asterisk',

    ]


    const [buttonsState, setButtonsState] = useState({
        living: false,
        cocina: false,
        bedroom: false,
        fullscreen: false,
        hall: false,

        heater: false,
        music: false,
        tv: false,
        blackout: false,
        speaker: false,

        color: false,
        light2: false,
        light3: false,
        device1: false,
        device2: false
    });

    const showBlackout = (show) => {
        // if (show) {
        //     document.querySelector('.blackout').classList.add('show');
        // } else {
        //     document.querySelector('.blackout').classList.remove('show');
        // }
    }

    const toggleButton = (button) => {
        if (button === "fullscreen") {
            if (!document.fullscreenElement) {
                document.documentElement.requestFullscreen();
            } else {
                document.exitFullscreen();
            }
        }

        if (button === "blackout") {
            showBlackout(true)
        }

        // if (button === "color") {
        //     if (buttonsState[button]) {
        //         document.documentElement.style.setProperty('--active-color', '#ffa500d5');
        //     } else {
        //         document.documentElement.style.setProperty('--active-color', '#90EE90'); // Ejemplo de verde claro
        //     }
        // }

        // console.log(button)
        setButtonsState((prevState) => ({ ...prevState, [button]: !prevState[button] }));
    };

    return (
        <div className="button-grid">
            {Object.keys(buttonsState).map((button, idx) => (
                // <div key={idx} className="button-wrapper">
                <div key={idx}
                    className={`button-wrapper ${buttonsState[button] ? 'active' : ''}`}
                    onClick={() => toggleButton(button)}
                >
                    <i className={` ${icons[idx]}`}></i>
                    <div
                        // className={`button ${buttonsState[button] ? 'active' : ''}`}
                        className={`button `}
                    >
                        {/* {button.toUpperCase()} */}
                        {labels[idx].toUpperCase()}
                    </div>
                    {/* <Slider /> */}
                    {/* {(button === "music" || button === "tv")
                        && <Slider isActive={buttonsState[button]} />
                    } */}
                    {(button === "music" || button === "tv") &&
                        <Slider
                            isActive={buttonsState[button]}
                            sliderHeight={button === "music" ? '75%' : '30%'}
                        />
                    }

                </div>
            ))}
            <div className='blackout' onClick={() => showBlackout(false)}>  </div>
        </div>
    );
}

export default ButtonGrid;
