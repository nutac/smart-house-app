import React from 'react'

export default function Slider({ isActive, sliderHeight }) {
    if (!isActive) {
        return null;
    }
    const sliderStyle = {
        '--slider-level': sliderHeight
    };
    return (
        <div className='slider' style={sliderStyle}>
        </div>
    );

}