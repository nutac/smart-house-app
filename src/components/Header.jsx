import React from 'react';
import logo from '../assets/Logo-Smarthouse2.png';
import LockPanel from './LockPanel';


function Header() {
    return (
        <div className="header">
            {/* <h1>HOME</h1> */}
            <img src={logo} alt="Mi Imagen" />

            <LockPanel />

        </div>
    );
}

export default Header;
