import React from 'react';
// import 'font-awesome/css/font-awesome.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import './App.css'


import Header from './components/Header';
// import RightPanel from './components/RightPanel';
// import LockPanel from './components/LockPanel'; 

import ButtonGrid from './components/ButtonGrid';

function App() {
  return (
    <div className="app">
      <Header />
      <div className="content">
        <ButtonGrid />
        {/* <LockPanel /> */}
        {/* <RightPanel /> */}
        {/* <LockPanel /> */}

      </div>
    </div>
  );
}

export default App;
